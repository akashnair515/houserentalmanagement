/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.house.rental.managment.service;

import in.ac.gpckasaragod.house.rental.managment.ui.CustomerDetails;
import java.util.List;

/**
 *
 * @author student
 */
public class CustomerDetailsService {

    /**
     *
     * @param NAME
     * @param ADDRESS
     * @return
     */
    public String saveCustomer(String NAME,String ADDRESS);
    public CustomerDetailsService readCustomer(Integer Id);
    public List<CustomerDetails> getAllCustomer();

    /**
     *
     * @param id
     * @param NAME
     * @param ADDRESS
     * @return
     */
    public String updateCustomer(Integer id,String NAME,String ADDRESS);
    public String deleteCustomer(Integer id);
}
